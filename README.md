Extra Features:

1. Set style of cells to display students' information as "Subtitle", hence the name and degree of the student will both be displayed on the screen.

2. Add function to delete one row by swipe to the left in the main page.

3. Design a .xib file as one kind of table view cell and use it to display teamname

4. Converting “Add Info” screen to a storyboard and using constraints

5. Using global variables to store selected row's index and corresponding students' object, hence it will be easier to display the information on another screen.

6. Using a picker view controller to decide gender. When edit an existed student information, the default display position for picker is decided by his/her gender, that is to say, originally editted information.

7. When edit an existed student's information, the add info page will display the exsited information in the labels for modifying. When add a new student's information, the add info page will display nothing as all the labels are empty.