//
//  StudentInfo.swift
//  Homework 3
//
//  Created by Yuan on 2/8/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import UIKit

class PersonInfo : CustomStringConvertible {
    var description: String {
        return "Test."
    }
}

class StudentInfo: NSObject {
    //student's name
    var name : String = ""
    //student's gender
    var gender : String = ""
    // student's hometown
    var hometown : String = ""
    // degree working on currently
    var degree : String = ""
    // proficient programming languages
    var programming : String = ""
    // interests outside of school
    var interest : String = ""
    
    var image : UIImage?
    
    init(name : String, gender : String, hometown : String, degree : String, programming : String, interest : String) {
        self.name = name
        self.gender = gender
        self.hometown = hometown
        self.degree = degree
        self.programming = programming
        self.interest = interest
        
    }
    
    override var description : String {
        if (gender == "Male") {
            return "\(self.name) is from \(self.hometown). He is working on \(self.degree) degree. He is proficient in programming languages such as \(self.programming). Additionally, he enjoys \(self.interest) outside of school."
        } else {
            return "\(self.name) is from \(self.hometown). She is working on \(self.degree) degree. She is proficient in programming languages such as \(self.programming). Additionally, she enjoys \(self.interest) outside of school."
        }
    }


}
