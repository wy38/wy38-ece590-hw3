//
//  SecondViewController.swift
//  Homework 3
//
//  Created by Yuan on 1/31/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var selfie: UIImageView!
    
    // button to edit information
    @IBOutlet weak var editInfoButton: UIBarButtonItem!
    
    // label of result to be displayed
    @IBOutlet weak var result: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ((sender as! UIBarButtonItem) != self.editInfoButton) {
            return
        }
        isNew = false
    }

    
    // display the output string and image
    override func viewDidAppear(_ animated: Bool) {
        result.text = stuInfo_global.description
        selfie.image = stuInfo_global.image
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
