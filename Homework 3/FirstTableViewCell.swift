//
//  FirstTableViewCell.swift
//  Homework 3
//
//  Created by Yuan on 2/10/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import UIKit

class FirstTableViewCell: UITableViewCell {
    
    @IBOutlet weak var image1: UIImageView!
    
    @IBOutlet weak var label1: UILabel!
}
