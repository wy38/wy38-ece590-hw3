//
//  ViewController.swift
//  Homework 3
//
//  Created by Yuan on 1/30/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.gender_t.delegate = self
        self.gender_t.dataSource = self
        self.name_t.delegate = self
        self.from_t.delegate = self
        self.degree_t.delegate = self
        self.hobbies_t.delegate = self
        self.from_t.delegate = self
        //  if modifying the information already have
        if (isNew == false) {
            name_t.text! = stuInfo_global.name
            if (stuInfo_global.gender == "Male") {
                gender_t.selectRow(0, inComponent: 0, animated: false)
            } else {
                gender_t.selectRow(1, inComponent: 0, animated: false)
            }
            from_t.text! = stuInfo_global.hometown
            degree_t.text! = stuInfo_global.degree
            programming_t.text! = stuInfo_global.programming
            hobbies_t.text! = stuInfo_global.interest
            image_t.image = stuInfo_global.image
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    // label of name
    @IBOutlet weak var name_t: UITextField!
    // label of hometown
    @IBOutlet weak var from_t: UITextField!
    // label of degree
    @IBOutlet weak var degree_t: UITextField!
    // label of programming languages
    @IBOutlet weak var programming_t: UITextField!
    // label of hobbies
    @IBOutlet weak var hobbies_t: UITextField!
    
    // Use picker view to select gender
    var gender_selected : String = ""
    @IBOutlet weak var gender_t: UIPickerView!
    
    // gender selection
    let genders = ["Male", "Female"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            gender_selected = genders[row]
    }
    
    
    
    // button to update information
    @IBOutlet weak var updateInfo: UIBarButtonItem!
    
    // button to cancel modification
    @IBOutlet weak var cancelInfoButton: UIBarButtonItem!
    
    var stuInfo = StudentInfo(name : "", gender : "Male", hometown : "", degree : "", programming : "", interest : "")
    
    // update the information
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ((sender as! UIBarButtonItem) != self.updateInfo) && ((sender as! UIBarButtonItem) != self.cancelInfoButton) {
            return
        }
        if (sender as! UIBarButtonItem) == self.cancelInfoButton {
            isNew = true
            return
        }
        self.stuInfo.name = name_t.text!
        self.stuInfo.gender = gender_selected
        self.stuInfo.hometown = from_t.text!
        self.stuInfo.degree = degree_t.text!
        self.stuInfo.programming = programming_t.text!
        self.stuInfo.interest = hobbies_t.text!
        self.stuInfo.image = image_t.image
    }
    
    // hide keyboard when user touch outside the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // hide keyboard when press return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBOutlet weak var image_t: UIImageView!
    
    // function to take the picture
    @IBAction func takePic(_ sender: UIButton) {
        let cam = UIImagePickerControllerSourceType.camera
        if (!UIImagePickerController.isSourceTypeAvailable(cam)) {
            print("no camera")
            return
        }
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerControllerSourceType.camera
        image.allowsEditing = false
        self.present(image, animated: true)
    }
    
    // update and display the picture
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            image_t.image = image
            self.stuInfo.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }

    
}


