//
//  FirstTableViewController.swift
//  Homework 3
//
//  Created by Yuan on 2/8/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import UIKit

// global variable to store the information for student selected
var stuInfo_global = StudentInfo(name : "", gender : "Male", hometown : "", degree : "", programming : "", interest : "")

var isNew : Bool = true

var row : Int = 0

class FirstTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadInitialData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch(section) {
            case 0:
                return CGFloat.leastNormalMagnitude
            default:
                return CGFloat.leastNormalMagnitude
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch(section) {
            case 0:
                return CGFloat.leastNormalMagnitude
            default:
                return CGFloat.leastNormalMagnitude
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
            return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
            case 0:
                return 1
            default:
                return self.studentLists.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch(indexPath.section) {
        case 0:
            let cell = Bundle.main.loadNibNamed("FirstTableViewCell", owner: self, options: nil)?.first as! FirstTableViewCell
            cell.label1.text = teamLists
            cell.image1.image = UIImage(named:"background")
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "StudentInfoCell", for: indexPath)
            let tempStuInfo:StudentInfo = self.studentLists[indexPath.row]
            cell.imageView?.image = tempStuInfo.image
            cell.textLabel?.text = tempStuInfo.name
            cell.detailTextLabel?.text = tempStuInfo.degree
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
        stuInfo_global = self.studentLists[indexPath.row]
        row = indexPath.row
    }
    
    // Unwind Segue to Main
    @IBAction func unwindtoMain(Segue: UIStoryboardSegue) {
        
    }
    
    var teamLists : String = "WyW"
    
    var studentLists = [StudentInfo]()
    
    // Pre-populate the Table with “hardcoded” information
    func loadInitialData() {
        let stu1 = StudentInfo(name : "Wanxin Yuan", gender : "Female",
                    hometown : "China",  degree : "Master", programming : "C, C++ and Python", interest : "Listening to music, watching movies and travelling")
        stu1.image = UIImage(named: "Wanxin")
        studentLists.append(stu1)
        let stu2 = StudentInfo(name : "Wuyi Sun", gender : "Female",
                               hometown : "China",  degree : "Master", programming : "C, C++, Javascript and HTML", interest : "Watching movies and running")
        studentLists.append(stu2)
        stu2.image = UIImage(named: "Wuyi")
        let stu3 = StudentInfo(name : "Yuhan Liu", gender : "Female",
                               hometown : "China",  degree : "Master", programming : "Java, C++, C and C#", interest : "Reading books, listening to music and doing sports")
        studentLists.append(stu3)
        stu3.image = UIImage(named: "Yuhan")
        
    }
    
    
    // Add or modify a new student's information on the table
    @IBAction func unwindToList(segue: UIStoryboardSegue) {
        let source:ViewController = segue.source as! ViewController
        let stu:StudentInfo = source.stuInfo
        if (stu.name != "") {
            if (isNew) {
                self.studentLists.append(stu)
            } else {
                self.studentLists[row] = stu
            }
        }
        isNew = true
        self.tableView.reloadData()
    }
    
    // Function to delete rows
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            // remove the item from the data model
            studentLists.remove(at: indexPath.row)
            
            // delete the table view row
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
    }
}
